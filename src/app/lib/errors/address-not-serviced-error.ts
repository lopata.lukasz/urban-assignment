import { ErrorBase } from './error-base';

export class AddressNotServicedError extends ErrorBase {
  code = 'ADDRESS_NOT_SERVICED';
  statusCode = 400;

  constructor(public address: string) {
    super(`Address ${address} not serviced`);
  }

  toResponse(): object {
    return {
      status: this.code,
      search: this.address
    };
  }
}
