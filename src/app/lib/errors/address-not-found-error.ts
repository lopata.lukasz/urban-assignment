import { ErrorBase } from './error-base';

export class AddressNotFoundError extends ErrorBase {
  code = 'ADDRESS_NOT_FOUND';
  statusCode = 404;

  constructor(public address: string) {
    super(`Address ${address} not found`);
  }

  toResponse(): object {
    return {
      status: this.code,
      search: this.address
    };
  }
}
