import inPolygon from 'point-in-polygon';
import { FeatureCollection, Geometry } from 'geojson';
import { getGeoJson } from '../orm/service-areas';

export enum GeometryTypes {
  Point = 'Point',
  MultiPoint = 'MultiPoint',
  LineString = 'LineString',
  MultiLineString = 'MultiLineString',
  Polygon = 'Polygon',
  MultiPolygon = 'MultiPolygon',
  GeometryCollection = 'GeometryCollection'
}

export function findServiceArea(lat: number, lng: number): null | string {
  const geoJson = getGeoJson() as FeatureCollection;
  const point = [lng, lat]; // coordinates are in format [lng, lat]
  const feature = geoJson.features.find(feature => isInside(point, feature.geometry))
  return feature?.properties?.Name || null;
}

function isInside(point: number[], geometry: Geometry): boolean {
  switch (geometry.type) {
    case GeometryTypes.Polygon:
      return !!geometry.coordinates.find(coords => inPolygon(point, coords))
    default:
      throw Error('Not implemented');
  }
}
