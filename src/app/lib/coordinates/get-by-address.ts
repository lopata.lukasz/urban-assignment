import { AddressNotFoundError } from '../errors/address-not-found-error';
import { IAddress } from '../models/address';
import { findServiceArea } from '../service-areas';
import { AddressNotServicedError } from '../errors/address-not-serviced-error';
import { GeocodeProvider, ProviderName } from './providers/types';
import { ProvidersFactory } from './providers/providers-factory';
import { getEnvs } from '../../envs';

export async function getCoordinatesByAddress(address: string) {
  const providers = getProviders();
  const response = await pickFirstResponse(providers, address);
  if (!response) {
    throw new AddressNotFoundError(address);
  }

  const serviceArea = findServiceArea(response.lat, response.lng);
  if (!serviceArea) {
    throw new AddressNotServicedError(address);
  }

  return { ...response, serviceArea };
}

function getProviders() {
  const envs = getEnvs();
  const GMAPS_KEY = envs.GOOGLE_MAPS_KEY;
  const RADAR_KEY = envs.RADAR_KEY;
  const PROVIDER_NAMES = envs.ORDER.split(',') as ProviderName[];

  const providersFactory = new ProvidersFactory(GMAPS_KEY, RADAR_KEY);
  return providersFactory.getProviders(...PROVIDER_NAMES);
}

async function pickFirstResponse(
  providers: GeocodeProvider[],
  address: string,
): Promise<IAddress | null> {
  for (const provider of providers) {
    const response = await provider
      .provide(address)
      .catch(() => {
        // handles or logs error
      });
    if (response) {
      return response;
    }
  }
  return null;
}
