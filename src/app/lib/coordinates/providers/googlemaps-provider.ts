import {
  AddressType,
  Client,
  GeocodeResult,
  GeocodingAddressComponentType
} from '@googlemaps/google-maps-services-js';
import { IAddress } from '../../models/address';
import { GeocodeProvider, GeocodeProviderFactory } from './types';

export class GooglemapsProviderFactory implements GeocodeProviderFactory{
  constructor(private readonly apiKey: string) {}

  create(): GooglemapsProvider {
    const client = new Client();
    return new GooglemapsProvider(client, this.apiKey);
  }
}

export class GooglemapsProvider implements GeocodeProvider {
  constructor(
    private readonly client: Client,
    private readonly apiKey: string,
  ) {}

  async provide(address: string): Promise<IAddress | null> {
    const params = { address, key: this.apiKey };
    const res = await this.client.geocode({ params });
    const noResult = !res.data?.results || res.data.results.length === 0;
    if (noResult) {
      return null;
    }
    const [result] = res.data.results;
    return this.mapAddress(result);
  }

  private mapAddress(result: GeocodeResult): IAddress {
    const { lat, lng } = result.geometry.location;
    const comps = result.address_components;
    const address1 = comps.find((x) => x.types.includes(AddressType.route))?.long_name;
    const address2 = comps.find((x) => x.types.includes(AddressType.neighborhood))?.long_name;
    const city = comps.find((x) => x.types.includes(GeocodingAddressComponentType.postal_town))?.long_name;
    return { address1, address2, city, lat, lng };
  }
}
