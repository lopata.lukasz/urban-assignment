import axios, { AxiosInstance } from 'axios';
import { IAddress } from '../../models/address';
import { GeocodeProvider, GeocodeProviderFactory } from './types';

const RADAR_BASE_URL = 'https://api.radar.io/v1';
const GEOCODE_URL = '/geocode/forward';

export class RadarProviderFactory implements GeocodeProviderFactory{
  constructor(private readonly apiKey: string) {}

  create(): RadarProvider {
    const client = axios.create({
      baseURL: RADAR_BASE_URL,
      headers: { Authorization: this.apiKey },
    });
    return new RadarProvider(client);
  }
}

export class RadarProvider implements GeocodeProvider {
  constructor(private readonly client: AxiosInstance) {}

  async provide(query: string): Promise<IAddress | null> {
    const { data } = await this.client.get(GEOCODE_URL, { params: { query } });
    const noResults = !Array.isArray(data.addresses) || data.addresses.length === 0;
    if (noResults) {
      return null;
    }
    const [result] = data.addresses;
    return this.mapResult(result);
  }

  private mapResult(result: Result): IAddress {
    const lat = result.latitude;
    const lng = result.longitude;
    const city = result.city;
    const address1 = result.addressLabel;
    const address2 = result.formattedAddress;
    return { lat, lng, city, address1, address2 };
  }
}

interface Result {
  latitude: number,
  longitude: number,
  city: string,
  formattedAddress: string,
  addressLabel: string
}
