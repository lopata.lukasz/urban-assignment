import { IAddress } from '../../models/address';

export enum ProviderName {
  GOOGLE_MAPS = "GOOGLE_MAPS",
  RADAR = "RADAR",
}

export interface GeocodeProvider {
  provide(address: string): Promise<IAddress | null>;
}

export interface GeocodeProviderFactory {
  create(): GeocodeProvider;
}
