import { GooglemapsProviderFactory } from './googlemaps-provider';
import { RadarProviderFactory } from './radar-provider';
import { ProviderName, GeocodeProvider } from './types';

export class ProvidersFactory {
  private gmapsProviderFactory: GooglemapsProviderFactory;
  private radarProviderFactory: RadarProviderFactory;

  constructor(googleApiKey: string, radarApiKey: string) {
    this.gmapsProviderFactory = new GooglemapsProviderFactory(googleApiKey);
    this.radarProviderFactory = new RadarProviderFactory(radarApiKey);
  }

  getProviders(...providerNames: ProviderName[]): GeocodeProvider[] {
    const providers: GeocodeProvider[] = [];
    for (const name of providerNames) {
      providers.push(this.createProvider(name));
    }
    return providers;
  }

  createProvider(name: ProviderName): GeocodeProvider {
    switch (name) {
      case ProviderName.GOOGLE_MAPS:
        return this.gmapsProviderFactory.create();
      case ProviderName.RADAR:
        return this.radarProviderFactory.create();
    }
    throw Error(`Unknown provider name ${name}`);
  }
}
