import { GeoJSON } from 'geojson';

const json = require('../../../data/formatted-data.json');

export function getGeoJson() {
  return json as GeoJSON;
}
