import { ErrorRequestHandler } from 'express';
import { ErrorBase } from './lib/errors/error-base';

const UNKNOWN_ERROR_STATUS_CODE = 500;
const UNKNOWN_ERROR_MESSAGE = 'General server error';

export function createErrorHandler(): ErrorRequestHandler {
  return (err, req, res, _) => {
    if (err instanceof ErrorBase) {
      return res
        .status(err.statusCode)
        .json(err.toResponse());
    }

    return res
      .status(UNKNOWN_ERROR_STATUS_CODE)
      .json({ message: UNKNOWN_ERROR_MESSAGE });
  };
}
