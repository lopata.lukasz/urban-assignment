import { cleanEnv, str, num } from 'envalid'
import { config } from 'dotenv';

config();

export function getEnvs() {
  return cleanEnv(process.env, {
    PORT: num({ default: 9000  }),
    GOOGLE_MAPS_KEY: str(),
    RADAR_KEY: str(),
    ORDER: str({ default: 'GOOGLE_MAPS,RADAR' })
  });
}
