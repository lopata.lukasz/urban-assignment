import { json } from 'body-parser';
import express from 'express';
import 'express-async-errors';
import { controller as geolocationController } from './controllers/geo-location';
import { createErrorHandler } from './error-handler';
import { getEnvs } from './envs';

const PORT = getEnvs().PORT;

export const app = express();
app.use(json());
geolocationController(app);
app.use(createErrorHandler());
app.listen(PORT, () => {
  console.log(`Listening on ${PORT}`);
});
