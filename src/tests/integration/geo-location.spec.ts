import { expect } from 'chai';
import sinon from 'sinon';
import request from 'supertest';
import { app } from '../../app';
import { features, stubGetGeo } from '../fixutes';
import { ProvidersFactory } from '../../app/lib/coordinates/providers/providers-factory';

describe('geo-location', () => {
  let provider: { provide: sinon.SinonStub };
  let getProvidersStub: sinon.SinonStub;

  beforeEach(() => {
    provider = { provide: sinon.stub() };
    getProvidersStub = sinon.stub(ProvidersFactory.prototype, 'getProviders');
    getProvidersStub.returns([provider]);
    stubGetGeo(features);
  });

  afterEach(() => {
    sinon.restore();
  })

  it('should return a valid service area', async () => {
    provider.provide.resolves({
      lat: 51.547133,
      lng: -0.005668,
      address1: 'testing address1',
      address2: 'testing address2',
      city: 'LONDON',
    });

    const { status, body } = await request(app)
      .get('/geolocation?address=testingaddress')
      .send();

    expect(status).to.eq(200);
    expect(body).to.deep.eq({
      search: 'testingaddress',
      status: 'OK',
      location: {
        address1: 'testing address1',
        address2: 'testing address2',
        city: 'LONDON',
        lat: 51.547133,
        lng: -0.005668,
        serviceArea: 'LONDONEAST',
      },
    });
  });

  it("Returns ADDRESS_NOT_FOUND when incorrect address given", async () => {
    provider.provide.resolves(null);

    const { status, body } = await request(app)
      .get('/geolocation?address=wrong_address')
      .send();

    expect(status).to.eq(404);
    expect(body).to.deep.eq({
      "status": "ADDRESS_NOT_FOUND",
      "search": "wrong_address"
    });
  });

  it("Returns ADDRESS_NOT_SERVICED when address not serviced", async () => {
    provider.provide.resolves({
      address1: '',
      address2: '',
      lat: 0.547133,
      lng: -0.005668,
      city: null
    });

    const { status, body } = await request(app)
      .get('/geolocation?address=a_not_serviced_address')
      .send();

    expect(status).to.eq(400);
    expect(body).to.deep.eq({
      "status": "ADDRESS_NOT_SERVICED",
      "search": "a_not_serviced_address"
    });
  })
});
