import { FeatureCollection, GeoJSON } from 'geojson';
import * as sinon from 'sinon';
import * as orm from '../app/orm/service-areas';
import { GeometryTypes } from '../app/lib/service-areas';

export function stubGetGeo(features: GeoJSON[]) {
  const result = { features } as FeatureCollection;
  sinon.stub(orm, 'getGeoJson').returns(result);
}

export const features: GeoJSON[] = [
  {
    type: 'Feature',
    properties: {
      Name: 'LONDONEAST',
    },
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [-0.00723123550415039, 51.5459347222684],
          [-0.0034761428833007812, 51.5459347222684],
          [-0.0034761428833007812, 51.54765609764073],
          [-0.00723123550415039, 51.54765609764073],
          [-0.00723123550415039, 51.5459347222684],
        ],
      ],
    },
  },
  {
    type: 'Feature',
    properties: {
      Name: 'LONDONCENTRAL',
    },
    geometry: {
      type: 'Polygon',
      coordinates: [
        [
          [-0.10884404182434082, 51.50659222851966],
          [-0.10638713836669922, 51.50659222851966],
          [-0.10638713836669922, 51.507273368348315],
          [-0.10884404182434082, 51.507273368348315],
          [-0.10884404182434082, 51.50659222851966],
        ],
      ],
    },
  },
];

const notImplementedGeometryType = GeometryTypes.Point;
export const featuresWithNotImplementedGeometry: GeoJSON[]  = [
  {
    type: 'Feature',
    properties: { Name: 'name stub' },
    geometry: {
      type: notImplementedGeometryType,
      coordinates: [],
    },
  },
]

export const googleMapsResults = [
  {
    "address_components": [
      {
        "long_name": "London",
        "short_name": "London",
        "types": [
          "locality",
          "political"
        ]
      },
      {
        "long_name": "London",
        "short_name": "London",
        "types": [
          "postal_town"
        ]
      },
      {
        "long_name": "Greater London",
        "short_name": "Greater London",
        "types": [
          "administrative_area_level_2",
          "political",
          "neighborhood"
        ]
      },
      {
        "long_name": "England",
        "short_name": "England",
        "types": [
          "route",
          "administrative_area_level_1",
          "political"
        ]
      },
      {
        "long_name": "United Kingdom",
        "short_name": "GB",
        "types": [
          "country",
          "political"
        ]
      }
    ],
    "formatted_address": "London, UK",
    "geometry": {
      "bounds": {
        "northeast": {
          "lat": 51.6723432,
          "lng": 0.148271
        },
        "southwest": {
          "lat": 51.38494009999999,
          "lng": -0.3514683
        }
      },
      "location": {
        "lat": 51.5072178,
        "lng": -0.1275862
      },
      "location_type": "APPROXIMATE",
      "viewport": {
        "northeast": {
          "lat": 51.6723432,
          "lng": 0.148271
        },
        "southwest": {
          "lat": 51.38494009999999,
          "lng": -0.3514683
        }
      }
    },
    "place_id": "ChIJdd4hrwug2EcRmSrV3Vo6llI",
    "types": [
      "locality",
      "political"
    ]
  }
];
