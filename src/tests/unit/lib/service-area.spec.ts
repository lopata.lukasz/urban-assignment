import { expect } from 'chai';
import * as sinon from 'sinon';
import { findServiceArea } from '../../../app/lib/service-areas';
import { features, featuresWithNotImplementedGeometry, stubGetGeo } from '../../fixutes';


describe('lib/service-areas', () => {
  afterEach(() => {
    sinon.restore();
  });

  it('should return a service area name if one exists', () => {
    stubGetGeo(features)
    const serviceAreaName = findServiceArea(51.547133, -0.005668);
    expect(serviceAreaName).to.eq('LONDONEAST');
  });

  it('should return null when there is no service area', () => {
    stubGetGeo(features)
    const serviceAreaName = findServiceArea(51.535534, -0.029012);
    expect(serviceAreaName).to.be.null;
  });

  it("Throws Error when not implemented geometry met", () => {
    stubGetGeo(featuresWithNotImplementedGeometry)
    const aCall = () => findServiceArea(51.535534, -0.029012);
    expect(aCall).to.throw('Not implemented');
  });
});
