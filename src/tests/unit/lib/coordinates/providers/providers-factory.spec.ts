import { ProvidersFactory } from '../../../../../app/lib/coordinates/providers/providers-factory';
import { ProviderName } from '../../../../../app/lib/coordinates/providers/types';
import { expect } from 'chai';
import { GooglemapsProvider } from '../../../../../app/lib/coordinates/providers/googlemaps-provider';
import { RadarProvider } from '../../../../../app/lib/coordinates/providers/radar-provider';

describe('ProvidersFactory', () => {
  const gmapsKey = 'a gmaps key stub'
  const radarKey = 'a Radar key stub';

  let factory: ProvidersFactory;

  beforeEach(() => {
    factory = new ProvidersFactory(gmapsKey, radarKey);
  })

  it('Returns only GmapsProvider when only that one is asked for', () => {
    const providers = factory.getProviders(ProviderName.GOOGLE_MAPS);
    expect(providers).to.have.length(1);
    expect(providers[0]).to.be.instanceof(GooglemapsProvider);
  });

  it('Returns only Radar when only that one is asked for', () => {
    const providers = factory.getProviders(ProviderName.RADAR);
    expect(providers).to.have.length(1);
    expect(providers[0]).to.be.instanceof(RadarProvider);
  });

  it('Keeps the correct order of providers', () => {
    const providers = factory.getProviders(ProviderName.RADAR, ProviderName.GOOGLE_MAPS);
    expect(providers).to.have.length(2);
    expect(providers[0]).to.be.instanceof(RadarProvider);
    expect(providers[1]).to.be.instanceof(GooglemapsProvider);

    const providers2 = factory.getProviders(ProviderName.GOOGLE_MAPS, ProviderName.RADAR);
    expect(providers2).to.have.length(2);
    expect(providers2[0]).to.be.instanceof(GooglemapsProvider);
    expect(providers2[1]).to.be.instanceof(RadarProvider);
  });
});
