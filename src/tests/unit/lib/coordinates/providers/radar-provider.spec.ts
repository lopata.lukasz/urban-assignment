import sinon from 'sinon';
import { RadarProvider } from '../../../../../app/lib/coordinates/providers/radar-provider';
import { expect } from 'chai';

describe("RadarProvider", () => {
  let mockedClient;
  let radar: RadarProvider;

  beforeEach(() => {
    mockedClient = { get: sinon.stub() };
    radar = new RadarProvider(mockedClient);
  });

  afterEach(() => {
    sinon.restore();
  });

  it('Returns null, when no results found', async () => {
    mockedClient.get.resolves({ data: { addresses: [] } });
    const result = await radar.provide("aquery");
    expect(result).to.be.null;
  });

  it('Returns null, when no addresses in the response', async () => {
    mockedClient.get.resolves({ data: {} });
    const result = await radar.provide("aquery");
    expect(result).to.be.null;
  });

  it("Maps addresses to IAddress", async () => {
    const exampleAddress =         {
      "latitude": 51.513263,
      "longitude": -0.089878,
      "geometry": {
        "type": "Point",
        "coordinates": [
          -0.089878,
          51.513263
        ]
      },
      "country": "United Kingdom",
      "countryCode": "GB",
      "countryFlag": "🇬🇧",
      "distance": 0,
      "confidence": "exact",
      "borough": "City of London",
      "city": "London",
      "state": "Greater London",
      "layer": "locality",
      "formattedAddress": "City of London, London, GBR",
      "addressLabel": "City of London"
    };
    mockedClient.get.resolves({ data: { addresses: [ exampleAddress ] } });

    const result = await radar.provide("aquery");
    expect(result).to.be.deep.eq({
      lat: exampleAddress.latitude,
      lng: exampleAddress.longitude,
      city: exampleAddress.city,
      address1: exampleAddress.addressLabel,
      address2: exampleAddress.formattedAddress
    });
  });
});
