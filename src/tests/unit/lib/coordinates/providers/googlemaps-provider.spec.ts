import sinon, { SinonStub } from 'sinon';
import { GooglemapsProvider } from '../../../../../app/lib/coordinates/providers/googlemaps-provider';
import { Client } from '@googlemaps/google-maps-services-js';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import { googleMapsResults } from '../../../../fixutes';

chai.use(sinonChai);

describe('GooglemapsProvider', () => {
  const client = new Client();
  const key = 'anApiKeyStub';

  let gmapsProvider: GooglemapsProvider;
  let geocodeStub: SinonStub;

  beforeEach(() => {
    geocodeStub = sinon.stub(client, 'geocode');
    gmapsProvider = new GooglemapsProvider(client, key);
  });

  afterEach(() => {
    sinon.restore();
  });

  it('Calls gmaps client with address and key', async () => {
    const address = 'a Searched address';
    geocodeStub.resolves({ data: {} });

    await gmapsProvider.provide(address);

    expect(geocodeStub).to.have.been.calledWith({ params: { address, key } });
  });

  it ('Returns null when no results', async () => {
    const address = 'a Searched address';
    geocodeStub.resolves({ data: {} });

    const result = await gmapsProvider.provide(address);

    expect(result).to.be.null;
  });

  it('Maps result to IAddress', async () => {
    const address = 'a Searched address';
    geocodeStub.resolves({
      data: {
        status: "OK",
        results: googleMapsResults,
      },
    });

    const result = await gmapsProvider.provide(address);
    expect(result).to.be.deep.eq({
      address1: 'England',
      address2: 'Greater London',
      city: 'London',
      lat: 51.5072178,
      lng: -0.1275862
    });
  })
});
