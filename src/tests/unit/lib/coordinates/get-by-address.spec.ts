import sinon from 'sinon';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import chaiAsPromised from 'chai-as-promised';
import { getCoordinatesByAddress } from '../../../../app/lib/coordinates/get-by-address';
import { GeocodeProvider } from '../../../../app/lib/coordinates/providers/types';
import * as findServiceAreaModule from '../../../../app/lib/service-areas';
import { AddressNotFoundError } from '../../../../app/lib/errors/address-not-found-error';
import { AddressNotServicedError } from '../../../../app/lib/errors/address-not-serviced-error';
import { ProvidersFactory } from '../../../../app/lib/coordinates/providers/providers-factory';

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe("get-by-address", () => {
  let findServiceAreaStub: sinon.SinonStub;
  let getProvidersStub: sinon.SinonStub;

  beforeEach(() => {
    findServiceAreaStub = sinon.stub(findServiceAreaModule, 'findServiceArea');
    getProvidersStub = sinon.stub(ProvidersFactory.prototype, 'getProviders');
  });

  afterEach(() => {
    sinon.restore();
  });

  it('Gets the first provided result', async () => {
    findServiceAreaStub.returns('some serviceArea name stub');
    const provide = sinon.stub().resolves({});
    const provider = { provide } as GeocodeProvider;
    getProvidersStub.returns([provider]);

    await getCoordinatesByAddress('some address');

    expect(provide).to.have.been.called;
  });

  it('Calls next provider, when result unavailable', async () => {
    findServiceAreaStub.returns('some serviceArea name stub');
    const first = { provide: sinon.stub().resolves(null)} as GeocodeProvider;
    const second = { provide: sinon.stub().resolves({})} as GeocodeProvider;
    getProvidersStub.returns([first, second]);

    await getCoordinatesByAddress("some address");

    expect(second.provide).to.have.been.called;
  });

  it('Does not Call next provider, when result found in first', async () => {
    findServiceAreaStub.returns('some serviceArea name stub');
    const first = { provide: sinon.stub().resolves({}) } as GeocodeProvider;
    const second = { provide: sinon.stub().resolves(null) } as GeocodeProvider;
    getProvidersStub.returns([first, second]);

    await getCoordinatesByAddress("some address");

    expect(second.provide).not.to.have.been.called;
  });

  it('Throws AddressNotFound when no provider found a result', async () => {
    getProvidersStub.returns([]);
    const aCall = getCoordinatesByAddress('some address');
    await expect(aCall).to.eventually.be.rejectedWith(AddressNotFoundError);
  });

  it("Throws AddressNotServiced when no service area", async () => {
    const provider = { provide: sinon.stub().resolves({}) };
    getProvidersStub.returns([provider]);
    const aCall = getCoordinatesByAddress('some address');
    await expect(aCall).to.eventually.be.rejectedWith(AddressNotServicedError);
  });

  it('Calls next provider, when Error occurred by first one', async () => {
    findServiceAreaStub.returns('some serviceArea name stub');
    const first = { provide: sinon.stub().rejects(`Some error`)} as GeocodeProvider;
    const second = { provide: sinon.stub().resolves({})} as GeocodeProvider;
    getProvidersStub.returns([first, second]);

    await getCoordinatesByAddress("some address");

    expect(second.provide).to.have.been.called;
  });
});
