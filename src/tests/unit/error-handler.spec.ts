import sinon from 'sinon';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import { mockReq, mockRes } from 'sinon-express-mock';
import { ErrorRequestHandler, NextFunction } from 'express';
import { createErrorHandler } from '../../app/error-handler';
import { AddressNotFoundError } from '../../app/lib/errors/address-not-found-error';

chai.use(sinonChai);

describe('createErrorHandler', () => {
  let errorHandler: ErrorRequestHandler;
  let req: ReturnType<typeof mockReq>;
  let res: ReturnType<typeof mockRes>;
  let next: NextFunction;

  beforeEach(() => {
    errorHandler = createErrorHandler();
    req = mockReq();
    res = mockRes();
    next = sinon.stub();
  })

  it("Responds with status and response from ErrorBase errors", () => {
    const address = 'An Address';
    const err = new AddressNotFoundError(address);
    const errResponse = err.toResponse();

    errorHandler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(err.statusCode);
    expect(res.json).to.have.been.calledWith(errResponse);
  });

  it("Responds with general server error when unrecognized error", () => {
    const err = new Error("Some random error");

    errorHandler(err, req, res, next);

    expect(res.status).to.have.been.calledWith(500);
    expect(res.json).to.have.been.calledWith({
      message: 'General server error',
    });
  });
})
