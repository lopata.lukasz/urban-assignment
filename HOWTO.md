# Requirements
There are two ways to run this app:
1. Locally.
2. Using Docker.

## Requirements to running the app locally
* node (not sure about the minimal version, ideally 17.x)

## Requirements to running the app in Docker:
* docker

# Before you run
## Radar geocoding
One of the tasks was to implement alternative to Google Maps geocode.
The alternative was no specified so, I've chosen [RADAR](https://radar.com).

Before playing around of testing please visit [RADAR](https://radar.com),
register and acquire an `API Key`.
It's needed to run the app.

[Radar Documentation](https://radar.com/documentation/api).

# Configuration
The app uses two Geocode providers:
1. Google Maps
2. Radar

App is searching the result in certain order,
and returns the result from first found provider.

The default order is: Google, Radar, but this can be change via configuration.

Due to the above, the App has 3 configurations in environments variables:
1. `GOOGLE_MAPS_KEY` - Google Maps API key, no default value _required!_.
2. `RADAR_KEY` - Radar Api ket, no default value, _required!_.
3. `ORDER` - coma separated order of providers, possible values:(`GOOGLE_MAPS`, `RADAR`).
Default value: `GOOGLE_MAPS,RADAR`
In order to pick ONLY ONE geocode service, set the `ORDER` value to that service.
For example:`ORDER=RADAR` will enable only Radar and disable GoogleMaps completely.

# How to run locally:
1. Checkout git repo:
```shell
git checkout [url]
cp .env.example .env
```

2. App requires configuration of Google Maps API and Radar API,
you can find the relevant env variables in `.env.example`:
```shell
cp .env.example .env
```
PLEASE fill up the .env with relevant values.

3. In order to run the app in development mode (restarting after file change):
```shell
npm run start:dev
```

4. In order to build sources and run from them:
```shell
npm run build
...
npm run start
```

5. In order to run all tests:
```shell
npm run test
```

The result should be:
```shell
npm run test

> technical_test@1.0.0 test
> mocha -r ts-node/register 'src/tests/**/*.spec.ts' --exit

Listening on 9000


  geo-location
    ✓ should return a valid service area
    ✓ Returns ADDRESS_NOT_FOUND when incorrect address given
    ✓ Returns ADDRESS_NOT_SERVICED when address not serviced

  createErrorHandler
    ✓ Responds with status and response from ErrorBase errors
    ✓ Responds with general server error when unrecognized error

  get-by-address
    ✓ Gets the first provided result
    ✓ Calls next provider, when result unavailable
    ✓ Does not Call next provider, when result found in first
    ✓ Throws AddressNotFound when no provider found a result
    ✓ Throws AddressNotServiced when no service area
    ✓ Calls next provider, when Error occurred by first one

  GooglemapsProvider
    ✓ Calls gmaps client with address and key
    ✓ Returns null when no results
    ✓ Maps result to IAddress

  ProvidersFactory
    ✓ Returns only GmapsProvider when only that one is asked for
    ✓ Returns only Radar when only that one is asked for
    ✓ Keeps the correct order of providers

  RadarProvider
    ✓ Returns null, when no results found
    ✓ Returns null, when no addresses in the response
    ✓ Maps addresses to IAddress

  lib/service-areas
    ✓ should return a service area name if one exists
    ✓ should return null when there is no service area
    ✓ Throws Error when not implemented geometry met


  23 passing (73ms)
```

# How to run in Docker
In order to build docker image:
```shell
docker build  . --tag=urban
```

When the image is build, in order to run the App:
```shell
docker run -e PORT=9000 -e GOOGLE_MAPS_KEY=... -e RADAR_KEY=... -e ORDER=RADAR,GOOGLE_MAPS -p 9000:9000 urban
```

# How to use the app:
When we have the app working locally, be it using local node, or docker,
in order to use we need to issue a request:
```shell
curl --location --request GET 'http://localhost:9000/geolocation?address=london'
```

The response should be:
```json
{
    "status": "OK",
    "search": "london",
    "location": {
        "lat": 51.513263,
        "lng": -0.089878,
        "city": "London",
        "address1": "City of London",
        "address2": "City of London, London, GBR",
        "serviceArea": "LONCENTRAL"
    }
}
```
