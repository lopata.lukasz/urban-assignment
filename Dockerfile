ARG PORT=8080
ARG GOOGLE_MAPS_KEY
ARG RADAR_KEY
ARG ORDER

FROM node:alpine as builder
WORKDIR /app
COPY . .
RUN npm ci
RUN npm run build

FROM node:alpine
ENV PORT=$PORT
ENV GOOGLE_MAPS_KEY=$GOOGLE_MAPS_KEY
ENV RADAR_KEY=$RADAR_KEY
ENV ORDER=$ORDER
WORKDIR /app
COPY . .
COPY --from=builder /app/dist ./dist
RUN npm ci --only=production
USER node
COPY --chown=node:node . .
EXPOSE $PORT
CMD ["npm", "run", "start"]
